const express = require( 'express' ),
  http = require( 'http' ),
  request = require( 'request' ),
  slackApi = require( './slack-api.js' ),
  PORT = process.env.PORT || 3000;
app = express();
slack = new slackApi();



// example from S/O https://stackoverflow.com/questions/32963736
// var options = {
//    host: 'www.google.com',
// path: '/index.html'
// };
// var req = http.get(options, function(res) {
//    console.log('STATUS: ' + res.statusCode);
//    console.log('HEADERS: ' + JSON.stringify(res.headers));
// Buffer the body entirely for processing as a whole.
//    var bodyChunks = [];
//    res.on('data', function(chunk) {
//          // You can process streamed parts here...
//          bodyChunks.push(chunk);
//        }).on('end', function() {
//              var body = Buffer.concat(bodyChunks);
//              console.log('BODY: ' + body);
//              // ...and/or process the entire body here.
//            })
//});
// req.on('error', function(e) {
//  console.log('ERROR: ' + e.message);
// });

// respond with "hello world" when a GET request is made to the homepage
app.post( '/chores', function ( req, res )
{
  console.log( "POST to /chores" )
  console.log( req.body )
  console.log( req.params )
  console.log( req.query )
  const chores = getChores()
  console.log( "got chores: " + chores )
  console.log( "Sending to slack..." )
  slack.postChores()
  console.log( "sent, returning confirmation" )
  res.send( 200 )
} )

app.listen( PORT, function ()
{
  console.log( 'running on ' + PORT + '...' );
} );


/** 
 * get the chores from jack's website
 */
function getChores ()
{
  request( 'https://jack-kenney.com/chores/json', function ( error, response, body )
  {
    // Print the error if one occurred
    console.error( 'error:', error );
    // Print the response status code if a response was received
    console.log( 'statusCode:', response && response.statusCode );
    // Print the HTML for the Google homepage.
    console.log( 'body:', body );

    return body;
  } )
}
