const request = require( 'request' ),
  https = require( 'https' )

class slackApi
{
  constructor () 
  {
    this.slackUrls = {
      privateRoom: "https://hooks.slack.com/services/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"
    }
  }

  /**
   * given the list of chores from jack's site, 
   * format them into a message, and send that message
   */
  postChores ( chores )
  {
    console.log( chores );
    const message = {
      'username': 'The Chore Knight',
      'text': "Here's the chores: "
    }

    this.sendSlackMessage( this.slackUrls.privateRoom, message );
  }


  /**
     * Handles the actual sending request. 
     * We're turning the https.request into a promise here for convenience
   * @param webhookURL
   * @param messageBody
   * @return {Promise}
   */
  sendSlackMessage ( webhookURL, messageBody )
  {
    // make sure the incoming message body can be parsed into valid JSON
    try
    {
      messageBody = JSON.stringify( messageBody );
    } catch ( e )
    {
      throw new Error( 'Failed to stringify messageBody', e );
    }

    // Promisify the https.request
    return new Promise( ( resolve, reject ) =>
    {
      // general request options, we defined that it's a POST request and content is JSON
      const requestOptions = {
        method: 'POST',
        header: {
          'Content-Type': 'application/json'
        }
      };

      // actual request
      const req = https.request( webhookURL, requestOptions, ( res ) =>
      {
        let response = '';


        res.on( 'data', ( d ) =>
        {
          response += d;
        } );

        // response finished, resolve the promise with data
        res.on( 'end', () =>
        {
          resolve( response );
        } )
      } );

      // there was an error, reject the promise
      req.on( 'error', ( e ) =>
      {
        reject( e );
      } );

      // send our message body (was parsed to JSON beforehand)
      req.write( messageBody );
      req.end();
    } );
  }
}

module.exports = slackApi
